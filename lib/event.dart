extension Comparison on DateTime
{
    bool operator < (DateTime other)
    {
        if (year < other.year)
        {
            return true;
        }
        else if (year == other.year && month < other.month)
        {
            return true;
        }
        else if (year == other.year && month == other.month && day < other.day)
        {
            return true;
        }
        else if (year == other.year && month == other.month && day == other.day && hour < other.hour)
        {
            return true;
        }
        else if (year == other.year && month == other.month && day == other.day && hour == other.hour && minute < other.minute)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool isSameInstant(DateTime other)
    {
        return (year == other.year && month == other.month && day == other.day && hour == other.hour && minute == other.minute);
    }
}

typedef EventUnitId = int;

class EventUnit
{
    DateTime startTime;
    DateTime endTime;

    EventUnit(this.startTime, this.endTime);

    bool isBefore(EventUnit other)
    {
        if (startTime < other.startTime)
        {
            return true;
        }
        else if (startTime.isSameInstant(other.startTime) && endTime < other.endTime)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

enum DayOfWeek
{
    monday,
    tuesday,
    wednesday,
    thursday,
    friday,
    saturday,
    sunday
}

enum PatternUnit
{
    day,
    week,
    month,
    year
}

class EventPattern
{
    PatternUnit frequencyUnit;
    int? frequencyValue;
    List<bool> daysOfWeek = List.filled(7, false);
    // int? nth;

    EventPattern.nTimesPerUnit({required this.frequencyValue, required this.frequencyUnit});
    EventPattern.someDaysEachWeek(Set<DayOfWeek> selectedDays) : frequencyUnit = PatternUnit.week
    {
        for (DayOfWeek dow in selectedDays)
        {
            daysOfWeek[dow.index] = true;
        }
    }
    // EventPattern.nthDayOfMonth()
}


class EventUnitStorer
{
    List<EventUnit> storage = [];

    EventUnitId addEvent(EventUnit newEventUnit)
    {
        EventUnitId newEventUnitId = storage.length;
        storage.add(newEventUnit);
        return newEventUnitId;
    }
}


class Event
{
    String name;
    List<EventUnitId> punctualOccurences = [];
    Map<EventUnitId, EventPattern> repeatedOccurences = <EventUnitId, EventPattern>{};

    Event.create(this.name);

    void addAnOccurence(DateTime startTime, DateTime endTime, EventUnitStorer storer)
    {
        EventUnit occurence = EventUnit(startTime, endTime);
        EventUnitId occurenceId = storer.addEvent(occurence);
        punctualOccurences.add(occurenceId);
    }

    void addARepeatedOccurence(DateTime startTime, DateTime endTime, EventPattern pattern, EventUnitStorer storer)
    {
        EventUnit repeatedOccurence = EventUnit(startTime, endTime);
        EventUnitId repeatedOccurenceId = storer.addEvent(repeatedOccurence);
        repeatedOccurences[repeatedOccurenceId] = pattern;
    }
}