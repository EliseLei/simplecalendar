import 'package:flutter/material.dart';
// import 'home_calendar.dart';
import 'tabs.dart';

void main() {
    runApp(const MyApp());
}

class MyApp extends StatelessWidget {
    const MyApp({super.key});

    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: "Mem'org",
            theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
            useMaterial3: true,
                ),
            home: const MyTabBar(),
        );
    }
}


// class TableCalendarRoute extends StatelessWidget
// {
//     const TableCalendarRoute({super.key});

//     @override
//     Widget build(BuildContext context)
//     {

//     }
// }