import 'package:flutter/material.dart';
// import 'package:table_calendar/table_calendar.dart';

final globalToday = DateTime.now();

class HomeCalendar extends StatefulWidget
{
    const HomeCalendar({super.key});

    final String title = "Simple calendar";

    @override
    State<HomeCalendar> createState() => _HomeCalendarState();
}

class _HomeCalendarState extends State<HomeCalendar>
{
    @override
    Widget build(BuildContext context)
    {
        return Scaffold(
            appBar: AppBar(
                backgroundColor: Theme.of(context).colorScheme.primary,
                title: Text(widget.title,
                style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),),
            ),

            body: GridView.count(
                crossAxisCount: 7,
                children: List.generate(7*24, (index) => Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 0.5),
                    ),
                    padding: const EdgeInsets.all(8.0),
                    alignment: Alignment.topRight,
                    child: Text('${index%7} - ${index~/7}h'),
                )),
            ),
        );
    }
}

Widget genGridview()
{
    return GridView.count(
        crossAxisCount: 7,
        children: List.generate(7*24, (index) => Container(
            decoration: BoxDecoration(
                border: Border.all(width: 0.5),
            ),
            padding: const EdgeInsets.all(8.0),
            alignment: Alignment.topRight,
            child: Text('${index%7} - ${index~/7}h'),
        )),
    );
}