import 'package:flutter/material.dart';
import 'table_calendar_example.dart';
import 'home_calendar.dart';

class MyTabBar extends StatelessWidget
{
    const MyTabBar({super.key});

    @override
    Widget build(BuildContext context)
    {
        return DefaultTabController(
            length: 3,
            child: Scaffold(
                appBar: AppBar(
                    backgroundColor: Theme.of(context).colorScheme.inversePrimary,
                    title: Text(
                        "Simple Calendar",
                        // style: TextStyle(color: Theme.of(context).colorScheme.on),
                    ),
                    bottom: const TabBar(
                        tabs: <Widget>[
                            Tab(icon: Icon(Icons.calendar_month)),
                            Tab(icon: Icon(Icons.table_chart_sharp)),
                            Tab(icon: Icon(Icons.format_align_justify_sharp)),
                        ],
                    ),
                ),

                body: TabBarView(
                    children: <Widget>[
                        const TableBasicsExample(),
                        genGridview(),
                        const Center(
                            child: Text("In progress..."),
                        ),
                    ],
                )
            )
        );
    }
}